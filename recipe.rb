class Vault < FPM::Cookery::Recipe
	name 'vault'
	version lookup('version')
	revision lookup('revision')

	description 'A tool for managing secrets.'
	license 'Mozilla Public License'
	vendor 'HashiCorp'
	homepage 'http://vaultproject.io'

	source "https://releases.hashicorp.com/#{name}/#{version}/#{name}_#{version}_linux_#{lookup('download_arch')}.zip"
	sha256 lookup('sha')

	config_files "/etc/#{name}"
	post_install 'post.sh'
	post_uninstall 'post.sh'

	def build
	end

	def install
		etc(name).install workdir('config.hcl')
		lib('systemd/system').install workdir('systemd.service'), "#{name}.service"
		bin.install name
	end
end
