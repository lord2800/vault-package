#!/bin/bash

set -e

SERVICE=vault

systemctl daemon-reload

case "$1" in
	configure)
		systemctl enable $SERVICE 2>&1 >/dev/null
		systemctl start $SERVICE 2>&1 > /dev/null
	;;
	remove)
		systemctl stop $SERVICE 2>&1 > /dev/null
	;;
    purge)
    	systemctl stop $SERVICE 2>&1 > /dev/null
    	systemctl disable $SERVICE 2>&1 >/dev/null || true
	;;
esac
