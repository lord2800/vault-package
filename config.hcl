ui = true

storage "consul" {
}

listener "tcp" {
    # disable TLS on the localhost interface
    tls_disable = 1
}

# TODO add global listener (have to generate a cert first)